using Guilded.Data.Forums;

namespace Guilded.Data.DAL.Forums
{
    interface IForumReadWriteRepository : IReadWriteRepository<Forum>
    {
    }
}
